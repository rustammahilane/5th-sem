import socket

HOST = "127.0.0.1"
PORT = 12345

def calculate(operand1, operand2, operator):
    if operator == '+':
        return operand1 + operand2
    elif operator == '-':
        return operand1 - operand2
    elif operator == '*':
        return operand1 * operand2
    elif operator == '/':
        if operand2 == 0:
            print("Invalid operand.")
        return operand1 / operand2
    elif operator == '%':
        if operand2 == 0:
            print("Invalid operand.")
        return operand1 % operand2

def main():
    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serverSocket.bind((HOST, PORT))
    serverSocket.listen(1)
    print("CalcServer is listening...")

    clientSocket, address = serverSocket.accept()
    print(f"Connection from {address} is established.")

    data = clientSocket.recv(1024).decode()
    operands = data.split()
    if len(operands) != 3:
        result = "Invalid input"
    else:
        operand1, operand2, operands = float(operands[0]), float(operands[1]), operands[2]
        result = calculate(operand1, operand2, operands)

    clientSocket.send(str(result).encode())
    clientSocket.close()

if __name__ == "__main__":
    main()