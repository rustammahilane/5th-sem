import socket

HOST = "127.0.0.1"
PORT = 12345

def main():
    clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientSocket.connect((HOST, PORT))

    operand1 = float(input("Enter operand1: "))
    operand2 = float(input("Enter operand2: "))
    operator = input("Enter operator {+,-,/,*,%}: ")

    request = f"{operand1} {operand2} {operator}"
    clientSocket.send(request.encode())
    result = clientSocket.recv(1024).decode()
    print(f"Result: {result}")

    clientSocket.close()

if __name__ == "__main__":
    main()