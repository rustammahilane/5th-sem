import socket
import re

HOST = "127.0.0.1"
PORT = 12345

def validPass(password):
    if len(password) < 8 or len(password) > 20:
        return False
    elif not re.search(r'[a-z]', password):
        return False
    elif not re.search(r'[A-Z]', password):
        return False
    elif not re.search(r'[0-9]', password):
        return False
    elif not re.search(r'[_@$]', password):
        return False
    return True

def main():
    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serverSocket.bind((HOST, PORT))
    serverSocket.listen(1)
    print("CalcServer is listening...")

    clientSocket, address = serverSocket.accept()
    print(f"Connection from {address} is established.")

    data = clientSocket.recv(1024).decode()
    if validPass(data):
        clientSocket.send("Valid Password".encode())
    else:
        clientSocket.send("INValid Password".encode())
    clientSocket.close()

if __name__ == "__main__":
    main()
