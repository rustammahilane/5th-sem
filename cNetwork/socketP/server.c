// connection-oriented server
#include <stdio.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/poll.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>

#define SERVER_PORT     3000
#define BUFFER_LENGTH   250
#define FALSE           0

void main()
{
    int     sd=-1, sd2=-1;
    int     rc, length, on=1;
    char    buffer[BUFFER_LENGTH];
    struct  pollfd fds;
    nfds_t  nfds = 1;
    int     timeout;
    struct sockaddr_in6 servaddr;


    do{
        sd = socket(AF_INET6, SOCK_STREAM, 0);
        if (sd < 0) {
            perror("socker() failed");
            break;
        }
        rc = setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on));
        if (rc < 0) {
            perror("setsocket(SO_REUSEADDR) failed");
            break;
        }

        memset(&servaddr, 0, sizeof(servaddr));
        servaddr.sin6_family = AF_INET6;
        servaddr.sin6_port   = htons(SERVER_PORT);
        memcpy(&servaddr.sin6_addr, &in6addr_any, sizeof(in6addr_any));

        rc = bind(sd, (struct sockaddr *) &servaddr, sizeof(servaddr));
        if (rc < 0) {
            perror("bind() failed");
            break;
        }

        rc = listen(sd, 10);
        if (rc < 0) {
            perror("listen() failed");
            break;
        }

        // 20 sec
        timeout = 20000;   

        memset(&fds, 0, sizeof(fds));
        fds.fd = sd2;
        fds.events = POLLIN;
        fds.revents = 0;

        rc = poll(&fds, nfds, timeout);

        if (rc < 0) {
            perror("poll() failed");
            break;
        }
        if (rc == 0) {
            printf("poll() timed out.\n");
            break;
        }
        length = BUFFER_LENGTH;
        rc = setsockopt(sd2, SOL_SOCKET, SO_RCVLOWAT,
                         (char *) &length, sizeof(length));

        if (rc < 0) {
            perror("setsockopt(SO_RCVLOWAT) failed");
            break;
        }
        
        // recive from cliet
        rc = recv(sd2, buffer, sizeof(buffer), 0);
        if (rc < 0) {
            perror("recv() failed");
            break;
        }

        printf("%d bytes of data were recieved\n", rc);
        if (rc == 0 || rc < sizeof(buffer)) {
            printf("The client closed the connection before all of the\n");
            printf("data was sent\n");
            break;
        }

        // echo the data back to the client
        rc = send(sd2, buffer, sizeof(buffer), 0);
        if (rc < 0) {
            perror("failed send()");
            break;
        }

        //program complete

    }while (FALSE);

    // close down any open socket descriptors
    if (sd != -1) close(sd);
    if (sd2 != -1) close(sd2);


}