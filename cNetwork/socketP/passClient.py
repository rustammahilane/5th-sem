import socket

HOST = "127.0.0.1"
PORT = 12345

def main():
    clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientSocket.connect((HOST, PORT))

    passw = input("Enter your password:")

    request = f"{passw}"
    clientSocket.send(request.encode())
    result = clientSocket.recv(1024).decode()
    print(f"Result: {result}")

    clientSocket.close()

if __name__ == "__main__":
    main()
