%{
    #include "q3.tab.h"
%}

%option noyywrap
%%
"main()" return MAIN;
"{"      return LBRACE;
"}"      return RBRACE;

[ \t\n]  return WHITESPACE;
.      return OTHER;
%%