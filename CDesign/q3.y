%{
    #include <stdio.h>
%}

%token MAIN LBRACE RBRACE OTHER WHITESPACE

%%
start: stmt MAIN ws LBRACE stmt RBRACE { printf("CORRECT syntax of main."); return 0; }
     ;
ws  : 
    | WHITESPACE ws
    ;
stmt: 
    | OTHER stmt
    | WHITESPACE stmt
    ;
%%

int yyerror(char *msg){
    fprintf(stderr, "%s, INCORRECT syntax on main", msg);
    return 1;
}
int main()
{
    yyparse();
    return 0;
}