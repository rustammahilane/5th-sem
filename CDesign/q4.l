%{
    #include "q4.tab.h"
%}

%option noyywrap

%%
"printf" return PRINT;
"scanf"  return SCAN;
"("      return LPAR;
")"      return RPAR;
","     return COMMA;
"&"     return AMPERSAND;
";"     return SEMICOLON;
[ \t]   return WHITESPACE;
\"[a-zA-Z0-9 \\%]*\"      return STRING;
[a-zA-Z_][a-zA-Z0-9_]*    return IDENTIFIER;
.   ;
%%