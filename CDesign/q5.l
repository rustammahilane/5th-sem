%{
    #include "q5.tab.h"
%}

%option noyywrap
%%

[ \n]	return WHITESPACE;
"if" 	    return IF;
"else"	return ELSE;
[a-zA-Z][_a-zA-Z0-9]* return IDENTIFIER;
[0-9]+	return NUMBER;
"&&"	return AND;
"||"	return OR;
">"	    return GT;
"<"	    return LT;
"=="	return EE;
"!="	return NE;
">="	return GE;
"<="	return LE;
"{"	return OB;
"}"	return CB;
"("	return LP;
")"	return RP;


%%