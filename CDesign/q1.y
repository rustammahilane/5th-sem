    /* write a yacc program to accept all  */
    /* strings of language L={a^nb^n: n>1} */

%{
    #include <stdio.h>
%}
    
%token A B

%%
start: expr '\n' { printf("is in anbn."); return 0; }
    ;
expr:A expr B
    |A B  
    ;
%%

int yyerror(char *msg){
    fprintf(stderr, "%s, is NOT anbn." , msg);
}

int main(){
    yyparse();
} 