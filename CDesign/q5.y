%{
	#include <stdio.h>
%}

%token IF ELSE LP RP IDENTIFIER NUMBER
%token LT GT EE NE GE LE OR OB CB AND
%token WHITESPACE

%%
start:ws IF ws LP condn ws RP ws body ws { printf("correct syntax if."); return 0; }
	|ws IF ws LP condn ws RP ws body ws ELSE ws body ws { printf("correct syntax ifelse."); return 0; }
	;
ws:
	| WHITESPACE ws
	;
condn:ws operand ws op ws operand ws
	| ws AND ws condn
	| ws OR ws condn
	;
operand: NUMBER
	| IDENTIFIER
	;
op: LE | GE | EE | NE | LT | GT ;

body: ws OB ws stmt ws CB ws
	;
stmt: ws operand ws
	| ws operand ws stmt
	;

%%

int yyerror(char *msg){
	fprintf(stderr, "%s", msg);
	return 1;
}

int main()
{
	yyparse();
	return 0;
}