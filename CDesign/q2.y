    /* E→E+E|E-E|E*E|E/E|digit */

%{
#include <stdio.h>
#include <stdlib.h>
%}

%token DIGIT
%left '+' '-'
%left '*' '/'

%%
arithmetic_operation: expression { printf("%d\n", $$); }
expression: expression '+' expression  { $$ = $1 + $3; }
          | expression '-' expression  { $$ = $1 - $3; }
          | expression '*' expression  { $$ = $1 * $3; }
          | expression '/' expression  { $$ = $1 / $3; }
          | DIGIT                    { $$ = $1 ; }
          ;

%%

int main() {
    yyparse();
    return 0;
}

int yyerror(char* message) {
    printf("%s, Invalid\n", message);
    return 1;
}
