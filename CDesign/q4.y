%{
    #include <stdio.h>
%}

%token PRINT SCAN LPAR RPAR STRING WHITESPACE
%token SEMICOLON IDENTIFIER COMMA AMPERSAND

%%

startP: PRINT LPAR STRING RPAR SEMICOLON { printf("correct printf funciotn."); return 0; }
    | PRINT LPAR STRING multistr RPAR SEMICOLON { printf("correct printf funciotn."); return 0; }
    | startS
    ;

multistr:ws COMMA ws IDENTIFIER
        |ws COMMA ws IDENTIFIER multistr
        ;
ws  : 
    | WHITESPACE ws
    ;

startS: SCAN LPAR STRING multiID RPAR SEMICOLON { printf("correct scanf funciotn."); return 0; }
    ;

multiID:ws COMMA ws AMPERSAND IDENTIFIER
    |ws COMMA ws AMPERSAND IDENTIFIER multiID
    ;
%%

int yyerror(char *msg){
    fprintf(stderr, "%s", msg);
    return 0;
}
int main()
{
    yyparse();
    return 0;
}