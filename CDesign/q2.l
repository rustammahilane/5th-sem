%{
    #include "q2.tab.h"
%}

%option noyywrap
%%
[0-9]+  { yylval = atoi(yytext); return DIGIT; }
[ ]  ;
[\t] ;
[\n] return 0;
. return yytext[0];
%%


